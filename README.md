# Machine Learning Playground

This repository holds Greg's scripts for various machine learning related
problems.

# Content

- [Sentiment classifier](sentiment/)
    - Linear Classifiers & Logistic Regression

# Contact

Greg Kamola <<greg.kamola@bcgdv.com>>, <<grzegorz.kamola@gmail.com>>
