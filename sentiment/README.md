# Sentiment classifier

166752 baby product reviews from Amazon were used for training.

| Model           | Accuracy |
| --------------- |:--------:|
| Sentiment       | 0.97     |
| Simple          | 0.87     |
| Majority voting | 0.84     |

## Dependencies

To install dependencies, type:

```pip install -r requirements.txt```

## Web demo

Run: 

```./server.py```

and open given hostname in the web browser.

## Training

Unzip data:

```unzip amazon-baby-products-reviews.zip```

Run training:

```./sentiment.py```
