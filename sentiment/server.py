#!/usr/bin/env python

from flask import Flask, request, jsonify
app = Flask(__name__)

from flask import render_template
from sentiment import *

DEMO_CLASSIFIER = None


@app.route('/')
def main():
    return render_template('demo.html')


@app.route('/predict', methods=['POST'])
def predict():
    feedback = request.json['feedback']
    probabilities = DEMO_CLASSIFIER.probabilities(feedback)[0]
    sentiment = 'Positive: %.2f, Negative: %.2f' % (probabilities[1], probabilities[0])

    print '\'%s\' classified as: %s' % (feedback, sentiment)
    return jsonify({
        'sentiment': sentiment
    })


if __name__ == "__main__":
    DEMO_CLASSIFIER = load_demo_classifier()
    app.run()