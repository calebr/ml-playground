#!/usr/bin/env python

import sframe

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer


def remove_punctuation(text):
    import string
    return text.translate(None, string.punctuation)


def handle_missing_data(data):
    return data.fillna('review', '') 


def remove_neutral_sentiment(data):
    return data[data['rating'] != 3]


def add_sentiment(data):
    data['sentiment'] = data['rating'].apply(lambda rating: 1 if rating > 3 else -1)
    return data


def load_review_data(path):
    data = sframe.SFrame(path)
    data['review_clean'] = data['review'].apply(remove_punctuation)
    
    data = handle_missing_data(data)
    data = remove_neutral_sentiment(data)
    data = add_sentiment(data)    

    return data.random_split(.8, seed=1)


class SentimentModel(object):
    def __init__(self):
        self.model = None
        self.vectorizer = None


    def fit(self, train_data, test_data):
        self.vectorizer = CountVectorizer(token_pattern=r'\b\w+\b')

        # First, learn vocabulary from the training data and assign columns to words
        # Then convert the training data into a sparse matrix
        train_matrix = self.vectorizer.fit_transform(train_data['review_clean'])

        # Second, convert the test data into a sparse matrix, using the same word-column mapping
        self.vectorizer.transform(test_data['review_clean'])

        self.model = LogisticRegression()
        return self.model.fit(X=train_matrix, y=train_data['sentiment'])


    def predict(self, text):
        return self.model.predict(self.vectorizer.transform([text]))


    def probabilities(self, text):
        return self.model.predict_proba(self.vectorizer.transform([text]))


class SimpleModel(object):
    '''
    Classifier base on fewer words, used for comparison.
    '''
    WORDS = ['love', 'great', 'easy', 'old', 'little', 'perfect', 'loves',
             'well', 'able', 'car', 'broke', 'less', 'even', 'waste', 'disappointed', 
             'work', 'product', 'money', 'would', 'return']

    def __init__(self):
        self.model = None
        self.vectorizer = None


    def fit(self, train_data, test_data):
        self.vectorizer = CountVectorizer(vocabulary=self.WORDS)

        train_matrix_word_subset = self.vectorizer.fit_transform(train_data['review_clean'])
        self.vectorizer.transform(test_data['review_clean'])
        
        self.model = LogisticRegression()
        return self.model.fit(X=train_matrix_word_subset, y=train_data['sentiment'])


    def predict(self, text):
        return self.model.predict(self.vectorizer.transform([text]))


    def probabilties(self, text):
        return self.model.predict_proba(self.vectorizer.transform([text]))


def round_accuracy(value):
    from decimal import Decimal
    return round(Decimal(value), 2)


class FileUtils(object):
    @staticmethod
    def save(data, path):
        from sklearn.externals import joblib
        return joblib.dump(data, path)


    @staticmethod
    def load(path):
        from sklearn.externals import joblib
        return joblib.load(path)


def experiments():
    sentiment_model = FileUtils.load('sentiment.model')

    # How many weights are >= 0?
    positive_coefficients = len(sentiment_model.model.coef_[sentiment_model.model.coef_ >= 0])
    print '%d weights are >= 0' % positive_coefficients

    # Of the three data points in sample_test_data, which one (first, second, or third) has the lowest probability of being classified as a positive review?
    train_data, test_data = load_review_data('amazon_baby.gl/')
    sample_test_data = test_data[10:13]

    print sample_test_data
    print 'Probabilities for three sample data points: #1: %.2f, #2: %.2f, #3: %.2f' % (
        sentiment_model.probabilities(sample_test_data[0]['review_clean'])[0][1],
        sentiment_model.probabilities(sample_test_data[1]['review_clean'])[0][1],
        sentiment_model.probabilities(sample_test_data[2]['review_clean'])[0][1])

    # Consider the coefficients of simple_model. How many of the 20 coefficients (corresponding to the 20 significant_words) are positive for the simple_model?
    simple_model = FileUtils.load('simple.model')
    positive_coefficients = len(simple_model.model.coef_[simple_model.model.coef_ > 0])
    print '%d coefficients are positive for simple model' % positive_coefficients

    # Are the positive words in the simple_model also positive words in the sentiment_model?
    # Answer: Yes
    for word, index in simple_model.vectorizer.vocabulary_.iteritems():
        sentiment_model_index = sentiment_model.vectorizer.vocabulary_[word]
        if sentiment_model_index:
            simple_coef = simple_model.model.coef_[0][index]
            sentiment_coef = sentiment_model.model.coef_[0][sentiment_model_index]

            if simple_coef > 0 and sentiment_coef <= 0:
                print 'Word: %s, has coeficient: %.2f in simple model and %.2f in sentiment model' % (word, simple_coef, sentiment_coef)

    # Which model (sentiment_model or simple_model) has higher accuracy on the TRAINING set?
    from sklearn.metrics import accuracy_score
    print 'Accuracy on training set: simple model: %f, sentiment model: %f' % (
        round_accuracy(accuracy_score(simple_model.model.predict(simple_model.vectorizer.transform(train_data['review_clean'])), train_data['sentiment'].to_numpy())),
        round_accuracy(accuracy_score(sentiment_model.model.predict(sentiment_model.vectorizer.transform(train_data['review_clean'])), train_data['sentiment'].to_numpy()))
    )

    # Which model (sentiment_model or simple_model) has higher accuracy on the TEST set?
    print 'Accuracy on test set: simple model: %f, sentiment model: %f' % (
        round_accuracy(accuracy_score(simple_model.model.predict(simple_model.vectorizer.transform(test_data['review_clean'])), test_data['sentiment'].to_numpy())),
        round_accuracy(accuracy_score(sentiment_model.model.predict(sentiment_model.vectorizer.transform(test_data['review_clean'])), test_data['sentiment'].to_numpy()))
    )

    # Enter the accuracy of the majority class classifier model on the test_data. Round your answer to two decimal places (e.g. 0.76).
    from sklearn.dummy import DummyClassifier
    majority_classifier = DummyClassifier(strategy='most_frequent')
    vectorizer = CountVectorizer(token_pattern=r'\b\w+\b')

    train_matrix_word_subset = vectorizer.fit_transform(train_data['review_clean'])
    vectorizer.transform(test_data['review_clean'])

    majority_classifier.fit(X=train_matrix_word_subset, y=train_data['sentiment'])
    print 'Majority classifier accuracy: %f' % (
        round_accuracy(accuracy_score(majority_classifier.predict(vectorizer.transform(train_data['review_clean'])), train_data['sentiment'].to_numpy())))

    # Is the sentiment_model definitely better than the majority class classifier (the baseline)?
    # Answer: yes, see accuracy

    # What is the accuracy of the sentiment_model on the test_data? Round your answer to 2 decimal places (e.g. 0.76).
    print 'Accuracy of the sentiment model on test data: %f' % (
        round_accuracy(accuracy_score(sentiment_model.model.predict(sentiment_model.vectorizer.transform(test_data['review_clean'])), test_data['sentiment'].to_numpy()))
    )

    # Does a higher accuracy value on the training_data always imply that the classifier is better?
    # Answer: No, it might mean that classifier is overfitted.

    # Which of the following products are represented in the 20 most negative reviews?
    probabilities = sentiment_model.model.predict_proba(sentiment_model.vectorizer.transform(train_data['review_clean']))
    print 'Products with most negative reviews: '
    print sorted(zip(train_data['name'], probabilities), key=lambda x: x[1][0])[-20:]

    # Which of the following products are represented in the 20 most positive reviews?
    print 'Products with most positive reviews: '
    print sorted(zip(train_data['name'], probabilities), key=lambda x: x[1][1])[-20:]


def load_demo_classifier():
    return FileUtils.load('sentiment.model')


def train_models():
    print 'Loading data...'
    train_data, test_data = load_review_data('amazon_baby.gl/')

    print 'Training sentiment model...'
    sentiment_model = SentimentModel()
    sentiment_model.fit(train_data, test_data)

    print 'Training simple model...'
    simple_model = SimpleModel()
    simple_model.fit(train_data, test_data)

    return (sentiment_model, simple_model)
    

if __name__ == '__main__':
    # experiments()
    (sentiment_model, simple_model) = train_models()

    print 'Saving models...'
    FileUtils.save(sentiment_model, 'sentiment.model')
    FileUtils.save(simple_model, 'simple.model')
